<?php

	function parser($path){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $path);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		$data = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		$dom = new DOMDocument;
		$dom->loadHTML($data);
		$links = $dom->getElementsByTagName('a');
		foreach ($links as $a) {
			$href = $a->getAttribute('href').PHP_EOL;
			$href = trim($href);
			if($href === '../' || $href === './')
				continue ;
			else{
				$new_path = $path.$href;

				if($href === 'README'){
					$cont = file_get_contents($new_path);
					file_put_contents('res.txt', $cont, FILE_APPEND);
				}
				else
					parser($new_path);
			}
		}
	}

	$path = $argv[1];
	parser($path);
?>